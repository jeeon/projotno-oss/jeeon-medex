import requests
from bs4 import BeautifulSoup
import json
from jsoncomment import JsonComment

output = open("output.json", "w+", encoding='utf-8')
output.truncate(0)
lines = output.readlines()

### Calculates number of pages, dynamic way
numberofpages = (BeautifulSoup(requests.get(f'https://medex.com.bd/brands').text, features="html.parser").find_all('li', class_='page-item')[-2]).text

### iteration for each page
for i in range (1):
    ### parse each page with bs4
    soup = BeautifulSoup((requests.get(f'https://medex.com.bd/brands?page={i}').text), features="html.parser")
    ### scraping each information with bs4
    productname = soup.find_all('div', class_='col-xs-12 data-row-top')
    productprice = soup.find_all('span', class_='package-pricing')
    productstrength = soup.find_all('span', class_='grey-ligten')
    # for rah in soup.find_all('span', class_='md-icon-container'):
    producttype = [rah.find('img').get('alt') for rah in soup.find_all('span', class_='md-icon-container') ]
    productgeneralname = [i.text.strip() for i in soup.find_all(lambda tag: tag.name == 'div' and tag.get('class') == ['col-xs-12'], text=True)]

    ### build json format with right keys
    for resname, resprice, resstrength, restype, resgeneralname in zip(productname, productprice, productstrength, producttype, productgeneralname):
            print(' ' + json.dumps({"product_name": resname.text.strip(), "product_price": resprice.text.replace('Unit Price : ','').replace('৳ ','').strip(), "product_quantity": resstrength.text.strip(), "product_type": restype, "product_genericname": resgeneralname}) + ',', file=output)

### format in correct json
output.seek(0) 
output.write('[')
for line in lines: 
    output.write(line)
output = open('output.json', 'a')
output.write(']')
output = open('output.json', 'r+')
dataaa = (JsonComment(json).load(output))
output = open('output.json', 'w')
print(json.dumps(dataaa), file=output)

